/*
 * io.h
 *
 *  Created on: 18 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_HOST_INTERFACE_IO_H_
#define SDR_HOST_INTERFACE_IO_H_

#include <SDR/BASE/common.h>
#include <SDR/BASE/Abstract.h>
#include <SDR/BASE/Multiplex/symMultiplex.h>

#include "base/host.h"
#include "base/target.h"

#ifndef SDR_HOST_IO_MUX_BUFSIZE
#define SDR_HOST_IO_MUX_BUFSIZE 32768
#endif

#ifdef __cplusplus
extern "C"{
#endif

typedef void (*HOST_IO_params_changed_t)(void * This, TARGET_Params_t * Params);
typedef void (*HOST_IO_set_value_t)(void * This, UInt8_t id, const char * val, Size_t size);
typedef struct
{
  void * Master;
  event_sink_t on_started, on_paused, on_continued, on_step, on_reset, on_stopped;
  event_sink_t on_get_all, on_format_request;
  HOST_IO_params_changed_t on_params_changed;
  HOST_IO_set_value_t on_set_value;
} HOST_IO_Cfg_t;

typedef void (*HOST_IO_rx_UserData_ready_t)(void * This, HOST_DataId_t id, UInt8_t * data, Size_t count);
typedef struct
{
  HOST_IO_rx_UserData_ready_t rx_UserData_ready;
} HOST_IO_Callback_t;
#define on_CBK(name) do{if(This->cfg.on_##name)This->cfg.on_##name(This->cfg.Master);}while(0)

typedef struct
{
  HOST_IO_Cfg_t cfg;

  void * Parent;
  HOST_IO_Callback_t cbk;

  symMultiplex_t txMux, rxMux;
  UInt8_t txMuxBuf[SDR_HOST_IO_MUX_BUFSIZE];
  UInt8_t rxTmpBuf[SDR_HOST_IO_MUX_BUFSIZE];

  Bool_t isStarted, isRunning;
} HOST_IO_t;

void init_HOST_IO(HOST_IO_t * This, HOST_IO_Cfg_t * Cfg);

INLINE TARGET_State_t HOST_IO_getState(HOST_IO_t * This)
{
  if (This->isStarted)
    return This->isRunning ? TARGET_Running : TARGET_Paused;
  return TARGET_Stopped;
}

INLINE Bool_t HOST_IO_isStarted(HOST_IO_t * This){return This->isStarted;}
INLINE Bool_t HOST_IO_isRunning(HOST_IO_t * This){return This->isStarted && This->isRunning;}

INLINE void HOST_IO_setCallback(HOST_IO_t * This, void * Parent, HOST_IO_Callback_t * cbk)
{
  This->Parent = Parent;
  This->cbk = *cbk;
}

Bool_t HOST_IO_send_uint8(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t data, Size_t bitsCount);
Bool_t HOST_IO_send_data(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t * Buf, Size_t Count);

Bool_t HOST_IO_send_response(HOST_IO_t * This, TARGET_Response_t Resp);
Bool_t HOST_IO_send_state_response(HOST_IO_t * This, TARGET_State_t State);

Bool_t HOST_IO_send_params(HOST_IO_t * This, TARGET_Params_t * params);
Bool_t HOST_IO_send_name(HOST_IO_t * This, const char * name);

Bool_t HOST_IO_send_format(HOST_IO_t * This, TARGET_DataId_t id, const char * format);

Bool_t HOST_IO_send_element_value(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * val);
Bool_t HOST_IO_send_element_params(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * params);
Bool_t HOST_IO_send_plot_command(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd);
Bool_t HOST_IO_send_plot_command_ext(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd, const char * ext);

Bool_t HOST_IO_send_samples(HOST_IO_t * This, TARGET_DataId_t id, Sample_t * Buf, Size_t Count);
Bool_t HOST_IO_send_samples_iq(HOST_IO_t * This, TARGET_DataId_t id, iqSample_t * Buf, Size_t Count);

INLINE void HOST_IO_setTargetState(HOST_IO_t * This, TARGET_State_t state)
{
  This->isStarted = (state != TARGET_Stopped);
  This->isRunning = (state == TARGET_Running);
  HOST_IO_send_state_response(This, state);
}

Bool_t HOST_IO_send_log_message(HOST_IO_t * This, const char * message);

INLINE void HOST_IO_startPaused(HOST_IO_t * This)
{
  This->isStarted = 1;
  This->isRunning = 0;

  on_CBK(started);

  HOST_IO_send_state_response(This, TARGET_Started);
}

INLINE void HOST_IO_continue(HOST_IO_t * This)
{
  SDR_ASSERT(This->isStarted);
  This->isRunning = 1;

  on_CBK(continued);

  HOST_IO_send_state_response(This, TARGET_Running);
}
INLINE void HOST_IO_start(HOST_IO_t * This)
{
  if (This->isStarted)
  {
    HOST_IO_continue(This);
    return;
  }

  This->isStarted = 1;
  This->isRunning = 1;

  HOST_IO_send_state_response(This, TARGET_Started);

  on_CBK(started);

  HOST_IO_send_state_response(This, TARGET_Running);
}
INLINE void HOST_IO_pause(HOST_IO_t * This)
{
  SDR_ASSERT(This->isStarted);
  This->isRunning = 0;

  on_CBK(paused);

  HOST_IO_send_state_response(This, TARGET_Paused);
}
INLINE void HOST_IO_step(HOST_IO_t * This)
{
  on_CBK(step);
}
INLINE void HOST_IO_reset(HOST_IO_t * This)
{
  on_CBK(reset);
}
INLINE void HOST_IO_stop(HOST_IO_t * This)
{
  This->isStarted = 0;
  This->isRunning = 0;

  HOST_IO_reset(This);

  on_CBK(stopped);

  HOST_IO_send_state_response(This, TARGET_Stopped);
}

#ifdef __cplusplus
}
#endif

#undef on_CBK

#endif /* SDR_HOST_INTERFACE_IO_H_ */
